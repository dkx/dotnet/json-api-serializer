using System.Collections.Generic;
using DKX.JsonApiSerializer.Resource;

namespace DKX.JsonApiSerializer.Relationship
{
	public class ItemRelationship : IRelationship
	{
		public ItemRelationship(Item item)
		{
			Item = item;
		}

		public Item Item { get; }

		public IEnumerable<Item> ToItems()
		{
			return new[] {Item};
		}
	}
}
