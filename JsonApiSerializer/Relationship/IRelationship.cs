using System.Collections.Generic;
using DKX.JsonApiSerializer.Resource;

namespace DKX.JsonApiSerializer.Relationship
{
	public interface IRelationship
	{
		IEnumerable<Item> ToItems();
	}
}
