using System.Collections.Generic;
using DKX.JsonApiSerializer.Resource;

namespace DKX.JsonApiSerializer.Relationship
{
	public class NullRelationship : IRelationship
	{
		public IEnumerable<Item> ToItems()
		{
			return new Item[] { };
		}
	}
}
