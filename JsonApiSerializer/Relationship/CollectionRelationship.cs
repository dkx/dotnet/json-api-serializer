using System.Collections.Generic;
using System.Linq;
using DKX.JsonApiSerializer.Resource;

namespace DKX.JsonApiSerializer.Relationship
{
	public class CollectionRelationship : IRelationship
	{
		public CollectionRelationship(IReadOnlyCollection<ItemRelationship> items)
		{
			Items = items;
		}

		public IReadOnlyCollection<ItemRelationship> Items { get; }

		public IEnumerable<Item> ToItems()
		{
			return Items.Select(item => item.Item);
		}
	}
}
