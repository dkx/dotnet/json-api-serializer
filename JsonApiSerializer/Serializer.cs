using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using DKX.JsonApiSerializer.Relationship;
using DKX.JsonApiSerializer.Resource;
using DKX.JsonApiSerializer.Value;

namespace DKX.JsonApiSerializer
{
	public class Serializer
	{
		private readonly SerializationOptions _options;
		
		public Serializer(SerializationOptions? options = null)
		{
			_options = options ?? new SerializationOptions();
		}
		
		public string Serialize(Document document)
		{
			using var stream = new MemoryStream();
			using (var writer = new Utf8JsonWriter(stream, _options.JsonOptions))
			{
				WriteDocument(writer, document);
			}

			return Encoding.UTF8.GetString(stream.ToArray());
		}

		private void WriteDocument(Utf8JsonWriter writer, Document document)
		{
			writer.WriteStartObject();
			
			if (document.Errors != null)
			{
				WriteErrorsList(writer, document.Errors);
			} else if (document.Data != null)
			{
				WriteRootResource(writer, document.Data);
				WriteIncludedItems(writer, document.Data);
			}

			if (document.Metadata != null)
			{
				WriteMeta(writer, document.Metadata);
			}
			
			writer.WriteEndObject();
		}

		private void WriteErrorsList(Utf8JsonWriter writer, ErrorsList errors)
		{
			writer.WriteStartArray("errors");

			foreach (var error in errors.Errors)
			{
				WriteError(writer, error);
			}
			
			writer.WriteEndArray();
		}

		private void WriteError(Utf8JsonWriter writer, Error error)
		{
			writer.WriteStartObject();

			if (error.Id != null)
			{
				writer.WriteString("id", error.Id);
			}

			if (error.AboutLink != null)
			{
				writer.WriteStartObject("links");
				writer.WriteString("about", error.AboutLink);
				writer.WriteEndObject();
			}

			if (error.Status != null)
			{
				writer.WriteString("status", error.Status);
			}

			if (error.Code != null)
			{
				writer.WriteString("code", error.Code);
			}

			if (error.Title != null)
			{
				writer.WriteString("title", error.Title);
			}

			if (error.Detail != null)
			{
				writer.WriteString("detail", error.Detail);
			}

			if (error.SourcePointer != null || error.SourceParameter != null)
			{
				writer.WriteStartObject("source");

				if (error.SourcePointer != null)
				{
					writer.WriteString("pointer", error.SourcePointer);
				}

				if (error.SourceParameter != null)
				{
					writer.WriteString("parameter", error.SourceParameter);
				}
				
				writer.WriteEndObject();
			}

			if (error.Metadata != null)
			{
				WriteMeta(writer, error.Metadata);
			}

			writer.WriteEndObject();
		}

		private void WriteRootResource(Utf8JsonWriter writer, IDocumentDataResource resource)
		{
			switch (resource)
			{
				case Null _: WriteRootNull(writer); break;
				case Item res: WriteRootItem(writer, res); break;
				case Collection res: WriteRootCollection(writer, res); break;
				default: throw new NotImplementedException();
			}
		}

		private static void WriteRootNull(Utf8JsonWriter writer)
		{
			WriteNullValue(writer, "data");
		}

		private void WriteRootItem(Utf8JsonWriter writer, Item item)
		{
			writer.WriteStartObject("data");
			WriteItem(writer, item);
			writer.WriteEndObject();
		}

		private void WriteRootCollection(Utf8JsonWriter writer, Collection collection)
		{
			writer.WriteStartArray("data");

			var items = collection.Items.AsEnumerable();
			if (_options.Sorted)
			{
				items = items
					.OrderBy(i => i.ResourceName)
					.ThenBy(i => i.Id);
			}

			foreach (var item in items)
			{
				writer.WriteStartObject();
				WriteItem(writer, item);
				writer.WriteEndObject();
			}
			
			writer.WriteEndArray();
		}

		private void WriteIncludedItems(Utf8JsonWriter writer, IDocumentDataResource resource)
		{
			var includeItems = GetIncludedItemsRecursive(resource).Distinct(new ItemComparer()).ToArray();
			if (includeItems.Length == 0)
			{
				return;
			}

			writer.WriteStartArray("included");

			if (_options.Sorted)
			{
				includeItems = includeItems
					.OrderBy(i => i.ResourceName)
					.ThenBy(i => i.Id)
					.ToArray();
			}

			foreach (var item in includeItems)
			{
				writer.WriteStartObject();
				WriteItem(writer, item);
				writer.WriteEndObject();
			}

			writer.WriteEndArray();
		}

		private void WriteItem(Utf8JsonWriter writer, Item item)
		{
			writer.WriteString("type", item.ResourceName);
			writer.WriteString("id", item.Id);

			if (item.SelfLink != null)
			{
				writer.WriteStartObject("links");
				writer.WriteString("self", item.SelfLink);
				writer.WriteEndObject();
			}

			if (item.Attributes.Data.Count > 0)
			{
				writer.WriteStartObject("attributes");

				var data = item.Attributes.Data.AsEnumerable();
				if (_options.Sorted)
				{
					data = data.OrderBy(d => d.Key);
				}

				foreach (var (key, attribute) in data)
				{
					WriteValue(writer, attribute, key);
				}
				
				writer.WriteEndObject();
			}

			if (item.Relationships.Count > 0)
			{
				writer.WriteStartObject("relationships");

				var relationships = item.Relationships.AsEnumerable();
				if (_options.Sorted)
				{
					relationships = relationships.OrderBy(r => r.Key);
				}

				foreach (var (name, relationship) in relationships)
				{
					WriteRelationship(writer, relationship, name);
				}
				
				writer.WriteEndObject();
			}
		}

		private void WriteMeta(Utf8JsonWriter writer, Meta meta)
		{
			if (meta.Fields.Data.Count == 0)
			{
				return;
			}

			writer.WriteStartObject("meta");

			var fields = meta.Fields.Data.AsEnumerable();
			if (_options.Sorted)
			{
				fields = fields.OrderBy(f => f.Key);
			}

			foreach (var (key, value) in fields)
			{
				WriteValue(writer, value, key);
			}
			
			writer.WriteEndObject();
		}

		private void WriteValue(Utf8JsonWriter writer, IValue value, string? name)
		{
			switch (value)
			{
				case Null _: WriteNullValue(writer, name); break;
				case BooleanValue val: WriteBooleanValue(writer, val, name); break;
				case NumberValue val: WriteNumberValue(writer, val, name); break;
				case StringValue val: WriteStringValue(writer, val, name); break;
				case List val: WriteListValue(writer, val, name); break;
				case Struct val: WriteStructValue(writer, val, name); break;
				default: throw new NotImplementedException();
			}
		}

		private static void WriteNullValue(Utf8JsonWriter writer, string? name)
		{
			if (name == null)
			{
				writer.WriteNullValue();
			}
			else
			{
				writer.WriteNull(name);
			}
		}

		private static void WriteBooleanValue(Utf8JsonWriter writer, BooleanValue value, string? name)
		{
			if (name == null)
			{
				writer.WriteBooleanValue(value.Value);
			}
			else
			{
				writer.WriteBoolean(name, value.Value);
			}
		}

		private static void WriteNumberValue(Utf8JsonWriter writer, NumberValue value, string? name)
		{
			if (name == null)
			{
				writer.WriteNumberValue(value.Value);
			}
			else
			{
				writer.WriteNumber(name, value.Value);
			}
		}

		private static void WriteStringValue(Utf8JsonWriter writer, StringValue value, string? name)
		{
			if (name == null)
			{
				writer.WriteStringValue(value.Value);
			}
			else
			{
				writer.WriteString(name, value.Value);
			}
		}

		private void WriteListValue(Utf8JsonWriter writer, List value, string? name)
		{
			if (name == null)
			{
				writer.WriteStartArray();
			}
			else
			{
				writer.WriteStartArray(name);
			}

			foreach (var item in value.Data)
			{
				WriteValue(writer, item, null);
			}
			
			writer.WriteEndArray();
		}

		private void WriteStructValue(Utf8JsonWriter writer, Struct value, string? name)
		{
			if (name == null)
			{
				writer.WriteStartObject();
			}
			else
			{
				writer.WriteStartObject(name);
			}

			var data = value.Data.AsEnumerable();
			if (_options.Sorted)
			{
				data = data.OrderBy(i => i.Key);
			}

			foreach (var (key, item) in data)
			{
				WriteValue(writer, item, key);
			}
			
			writer.WriteEndObject();
		}

		private void WriteRelationship(Utf8JsonWriter writer, IRelationship relationship, string name)
		{
			switch (relationship)
			{
				case NullRelationship _: WriteRelationshipNull(writer, name); break;
				case ItemRelationship rel: WriteRelationshipItem(writer, rel, name); break;
				case CollectionRelationship rel: WriteRelationshipCollection(writer, rel, name); break;
				default: throw new NotImplementedException();
			}
		}

		private static void WriteRelationshipNull(Utf8JsonWriter writer, string name)
		{
			writer.WriteStartObject(name);
			writer.WriteNull("data");
			writer.WriteEndObject();
		}

		private static void WriteRelationshipItem(Utf8JsonWriter writer, ItemRelationship relationship, string name)
		{
			writer.WriteStartObject(name);
			writer.WriteStartObject("data");
			writer.WriteString("type", relationship.Item.ResourceName);
			writer.WriteString("id", relationship.Item.Id);
			writer.WriteEndObject();
			writer.WriteEndObject();
		}

		private void WriteRelationshipCollection(Utf8JsonWriter writer, CollectionRelationship relationship, string name)
		{
			writer.WriteStartObject(name);
			writer.WriteStartArray("data");

			var items = relationship.Items.AsEnumerable();
			if (_options.Sorted)
			{
				items = items
					.OrderBy(r => r.Item.ResourceName)
					.ThenBy(r => r.Item.Id);
			}

			foreach (var item in items)
			{
				writer.WriteStartObject();
				writer.WriteString("type", item.Item.ResourceName);
				writer.WriteString("id", item.Item.Id);
				writer.WriteEndObject();
			}
			
			writer.WriteEndArray();
			writer.WriteEndObject();
		}

		private static IEnumerable<Item> GetIncludedItemsRecursive(IDocumentDataResource resource)
		{
			var result = new List<Item>();
			
			foreach (var item in resource.GetIncludedItems())
			{
				result.Add(item);
				result.AddRange(GetIncludedItemsRecursive(item));
			}

			return result;
		}

		private class ItemComparer : IEqualityComparer<Item>
		{
			public bool Equals(Item? a, Item? b)
			{
				if (a == null && b == null)
				{
					return true;
				}
				
				if (a == null && b != null)
				{
					return false;
				}

				if (a != null && b == null)
				{
					return false;
				}
				
				return a!.ResourceName == b!.ResourceName && a.Id == b.Id;
			}

			public int GetHashCode(Item item)
			{
				return HashCode.Combine(item.ResourceName, item.Id);
			}
		}
	}
}
