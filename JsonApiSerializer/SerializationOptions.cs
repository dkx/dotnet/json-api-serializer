using System.Text.Json;

namespace DKX.JsonApiSerializer
{
	public class SerializationOptions
	{
		public bool Sorted { get; set; }

		public JsonWriterOptions JsonOptions { get; set; }
	}
}
