using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace DKX.JsonApiSerializer.Value
{
	public class List : IValue
	{
		public List(IReadOnlyCollection<IValue>? data = null)
		{
			Data = data ?? ImmutableList<IValue>.Empty;
		}

		public IReadOnlyCollection<IValue> Data { get; }

		public List Add(IValue value)
		{
			return new List(Data.Append(value).ToImmutableList());
		}

		public List Add(string? value)
		{
			return Add(ValueFactory.Create(value));
		}

		public List Add(bool? value)
		{
			return Add(ValueFactory.Create(value));
		}

		public List Add(decimal? value)
		{
			return Add(ValueFactory.Create(value));
		}

		public List Add(double? value)
		{
			return Add(ValueFactory.Create(value));
		}

		public List Add(int? value)
		{
			return Add(ValueFactory.Create(value));
		}

		public List Add(long? value)
		{
			return Add(ValueFactory.Create(value));
		}

		public List Add(float? value)
		{
			return Add(ValueFactory.Create(value));
		}

		public List Add(uint? value)
		{
			return Add(ValueFactory.Create(value));
		}

		public List Add(ulong? value)
		{
			return Add(ValueFactory.Create(value));
		}

		public List Merge(List other)
		{
			var data = Data.AsEnumerable();
			foreach (var item in other.Data)
			{
				data = data.Append(item);
			}

			return new List(data.ToImmutableList());
		}
	}
}
