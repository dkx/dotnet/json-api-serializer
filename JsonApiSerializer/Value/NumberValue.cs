namespace DKX.JsonApiSerializer.Value
{
	public class NumberValue : IValue
	{
		public NumberValue(decimal value)
		{
			Value = value;
		}
		
		public NumberValue(double value)
		{
			Value = value;
		}
		
		public NumberValue(int value)
		{
			Value = value;
		}
		
		public NumberValue(long value)
		{
			Value = value;
		}
		
		public NumberValue(float value)
		{
			Value = value;
		}
		
		public NumberValue(uint value)
		{
			Value = value;
		}
		
		public NumberValue(ulong value)
		{
			Value = value;
		}
		
		public dynamic Value { get; }
	}
}
