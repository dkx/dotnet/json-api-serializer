using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace DKX.JsonApiSerializer.Value
{
	public class Struct : IValue
	{
		public Struct(IReadOnlyDictionary<string, IValue>? data = null)
		{
			Data = data ?? ImmutableDictionary<string, IValue>.Empty;
		}
		
		public IReadOnlyDictionary<string, IValue> Data { get; }

		public Struct Add(string name, IValue value)
		{
			var data = Data.Append(new KeyValuePair<string, IValue>(name, value));
			return new Struct(data.ToImmutableDictionary());
		}

		public Struct Add(string name, string? value)
		{
			return Add(name, ValueFactory.Create(value));
		}

		public Struct Add(string name, bool? value)
		{
			return Add(name, ValueFactory.Create(value));
		}

		public Struct Add(string name, decimal? value)
		{
			return Add(name, ValueFactory.Create(value));
		}

		public Struct Add(string name, double? value)
		{
			return Add(name, ValueFactory.Create(value));
		}

		public Struct Add(string name, int? value)
		{
			return Add(name, ValueFactory.Create(value));
		}

		public Struct Add(string name, long? value)
		{
			return Add(name, ValueFactory.Create(value));
		}

		public Struct Add(string name, float? value)
		{
			return Add(name, ValueFactory.Create(value));
		}

		public Struct Add(string name, uint? value)
		{
			return Add(name, ValueFactory.Create(value));
		}

		public Struct Add(string name, ulong? value)
		{
			return Add(name, ValueFactory.Create(value));
		}

		public Struct Merge(Struct other)
		{
			var result = Data.AsEnumerable();
			foreach (var (key, item) in other.Data)
			{
				result = result.Append(new KeyValuePair<string, IValue>(key, item));
			}

			return new Struct(result.ToImmutableDictionary());
		}
	}
}
