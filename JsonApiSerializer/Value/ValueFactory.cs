using System;
using DKX.JsonApiSerializer.Resource;

namespace DKX.JsonApiSerializer.Value
{
	public static class ValueFactory
	{
		public static IValue Create(bool? value)
		{
			return value == null ? (IValue) new Null() : new BooleanValue((bool) value);
		}
		
		public static IValue Create(decimal? value)
		{
			return value == null ? (IValue) new Null() : new NumberValue((decimal) value);
		}
		
		public static IValue Create(double? value)
		{
			return value == null ? (IValue) new Null() : new NumberValue((double) value);
		}
		
		public static IValue Create(int? value)
		{
			return value == null ? (IValue) new Null() : new NumberValue((int) value);
		}
		
		public static IValue Create(long? value)
		{
			return value == null ? (IValue) new Null() : new NumberValue((long) value);
		}
		
		public static IValue Create(float? value)
		{
			return value == null ? (IValue) new Null() : new NumberValue((float) value);
		}
		
		public static IValue Create(uint? value)
		{
			return value == null ? (IValue) new Null() : new NumberValue((uint) value);
		}
		
		public static IValue Create(ulong? value)
		{
			return value == null ? (IValue) new Null() : new NumberValue((ulong) value);
		}
		
		public static IValue Create(string? value)
		{
			return value == null ? (IValue) new Null() : new StringValue(value);
		}

		public static IValue Create(object? value)
		{
			return value switch
			{
				IValue val => val,
				null => new Null(),
				bool val => new BooleanValue(val),
				decimal val => new NumberValue(val),
				double val => new NumberValue(val),
				int val => new NumberValue(val),
				long val => new NumberValue(val),
				float val => new NumberValue(val),
				uint val => new NumberValue(val),
				ulong val => new NumberValue(val),
				string val => new StringValue(val),
				_ => throw new NotSupportedException($"Type {value.GetType()} is not supported")
			};
		}
	}
}
