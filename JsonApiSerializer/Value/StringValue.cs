namespace DKX.JsonApiSerializer.Value
{
	public class StringValue : IValue
	{
		public StringValue(string value)
		{
			Value = value;
		}
		
		public string Value { get; }
	}
}
