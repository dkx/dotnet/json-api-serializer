namespace DKX.JsonApiSerializer.Value
{
	public class BooleanValue : IValue
	{
		public BooleanValue(bool value)
		{
			Value = value;
		}
		
		public bool Value { get; }
	}
}
