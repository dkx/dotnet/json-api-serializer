using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using DKX.JsonApiSerializer.Relationship;
using DKX.JsonApiSerializer.Value;

namespace DKX.JsonApiSerializer.Resource
{
	public class Item : IDocumentDataResource, ICloneable
	{
		public Item(string resourceName, string id)
		{
			ResourceName = resourceName;
			Id = id;
		}
		
		public string ResourceName { get; }

		public string Id { get; }
		
		public string? SelfLink { get; private set; }

		public Struct Attributes { get; private set; } = new Struct();

		public IReadOnlyDictionary<string, IRelationship> Relationships { get; private set; } = ImmutableDictionary<string, IRelationship>.Empty;

		public Item WithSelfLink(string? link)
		{
			var clone = (Item) Clone();
			clone.SelfLink = link;

			return clone;
		}

		public Item WithAttributes(Struct attributes)
		{
			var clone = (Item) Clone();
			clone.Attributes = attributes;

			return clone;
		}

		public Item WithRelationships(IReadOnlyDictionary<string, IRelationship> relationships)
		{
			var clone = (Item) Clone();
			clone.Relationships = relationships;

			return clone;
		}

		public Item AddAttribute(string name, IValue value)
		{
			return WithAttributes(Attributes.Add(name, value));
		}

		public Item AddAttribute(string name, bool value)
		{
			return AddAttribute(name, new BooleanValue(value));
		}

		public Item AddAttribute(string name, double value)
		{
			return AddAttribute(name, new NumberValue(value));
		}

		public Item AddAttribute(string name, string value)
		{
			return AddAttribute(name, new StringValue(value));
		}

		public Item AddRelationship(string name, IRelationship? relationship)
		{
			var relationships = Relationships.Append(new KeyValuePair<string, IRelationship>(name, relationship ?? new NullRelationship()));
			return WithRelationships(relationships.ToImmutableDictionary());
		}

		public Item AddRelationship(string name, Item item)
		{
			return AddRelationship(name, new ItemRelationship(item));
		}

		public Item AddRelationship(string name, IEnumerable<Item> items)
		{
			return AddRelationship(name, new CollectionRelationship(items.Select(item => new ItemRelationship(item)).ToImmutableList()));
		}

		public IEnumerable<Item> GetIncludedItems()
		{
			var result = new List<Item>();
			foreach (var (_, relationship) in Relationships)
			{
				result.AddRange(relationship.ToItems());
			}

			return result;
		}

		public object Clone()
		{
			return new Item(ResourceName, Id)
			{
				SelfLink = SelfLink,
				Attributes = Attributes,
				Relationships = Relationships,
			};
		}
	}
}
