using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace DKX.JsonApiSerializer.Resource
{
	public class ErrorsList
	{
		public ErrorsList(IReadOnlyCollection<Error> errors)
		{
			Errors = errors;
		}

		public IReadOnlyCollection<Error> Errors { get; }

		public ErrorsList AddError(Error error)
		{
			return new ErrorsList(Errors.Append(error).ToImmutableArray());
		}
	}
}
