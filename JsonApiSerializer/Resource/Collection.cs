using System.Collections.Generic;

namespace DKX.JsonApiSerializer.Resource
{
	public class Collection : IDocumentDataResource
	{
		public Collection(IReadOnlyCollection<Item> items)
		{
			Items = items;
		}
		
		public IReadOnlyCollection<Item> Items { get; }

		public IEnumerable<Item> GetIncludedItems()
		{
			var result = new List<Item>();
			foreach (var item in Items)
			{
				result.AddRange(item.GetIncludedItems());
			}

			return result;
		}
	}
}
