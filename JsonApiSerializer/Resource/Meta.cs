using DKX.JsonApiSerializer.Value;

namespace DKX.JsonApiSerializer.Resource
{
	public class Meta
	{
		public Meta(Struct? fields = null)
		{
			Fields = fields ?? new Struct();
		}
		
		public Struct Fields { get; }

		public Meta Add(string name, IValue value)
		{
			return new Meta(Fields.Add(name, value));
		}

		public Meta Add(string name, string? value)
		{
			return Add(name, ValueFactory.Create(value));
		}

		public Meta Add(string name, bool? value)
		{
			return Add(name, ValueFactory.Create(value));
		}

		public Meta Add(string name, decimal? value)
		{
			return Add(name, ValueFactory.Create(value));
		}

		public Meta Add(string name, double? value)
		{
			return Add(name, ValueFactory.Create(value));
		}

		public Meta Add(string name, int? value)
		{
			return Add(name, ValueFactory.Create(value));
		}

		public Meta Add(string name, long? value)
		{
			return Add(name, ValueFactory.Create(value));
		}

		public Meta Add(string name, float? value)
		{
			return Add(name, ValueFactory.Create(value));
		}

		public Meta Add(string name, uint? value)
		{
			return Add(name, ValueFactory.Create(value));
		}

		public Meta Add(string name, ulong? value)
		{
			return Add(name, ValueFactory.Create(value));
		}

		public Meta Merge(Meta other)
		{
			return new Meta(Fields.Merge(other.Fields));
		}
	}
}
