using System.Collections.Generic;
using DKX.JsonApiSerializer.Value;

namespace DKX.JsonApiSerializer.Resource
{
	public class Null : IDocumentDataResource, IValue
	{
		public IEnumerable<Item> GetIncludedItems()
		{
			return new Item[] { };
		}
	}
}
