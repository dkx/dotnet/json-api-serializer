using System;

namespace DKX.JsonApiSerializer.Resource
{
	public class Error : ICloneable
	{
		public string? Id { get; private set; }

		public string? AboutLink { get; private set; }

		public string? Status { get; private set; }

		public string? Code { get; private set; }

		public string? Title { get; private set; }

		public string? Detail { get; private set; }

		public string? SourcePointer { get; private set; }

		public string? SourceParameter { get; private set; }

		public Meta? Metadata { get; private set; }

		public Error WithId(string? id)
		{
			var clone = (Error) Clone();
			clone.Id = id;

			return clone;
		}

		public Error WithAboutLink(string? aboutLink)
		{
			var clone = (Error) Clone();
			clone.AboutLink = aboutLink;

			return clone;
		}

		public Error WithStatus(string? status)
		{
			var clone = (Error) Clone();
			clone.Status = status;

			return clone;
		}

		public Error WithCode(string? code)
		{
			var clone = (Error) Clone();
			clone.Code = code;

			return clone;
		}

		public Error WithTitle(string? title)
		{
			var clone = (Error) Clone();
			clone.Title = title;

			return clone;
		}

		public Error WithDetail(string? detail)
		{
			var clone = (Error) Clone();
			clone.Detail = detail;

			return clone;
		}

		public Error WithSourcePointer(string? sourcePointer)
		{
			var clone = (Error) Clone();
			clone.SourcePointer = sourcePointer;

			return clone;
		}

		public Error WithSourceParameter(string? code)
		{
			var clone = (Error) Clone();
			clone.SourceParameter = code;

			return clone;
		}

		public Error WithMetadata(Meta? metadata)
		{
			var clone = (Error) Clone();
			clone.Metadata = metadata;

			return clone;
		}

		public object Clone()
		{
			return new Error
			{
				Id = Id,
				AboutLink = AboutLink,
				Status = Status,
				Code = Code,
				Title = Title,
				Detail = Detail,
				SourcePointer = SourcePointer,
				SourceParameter = SourceParameter,
				Metadata = Metadata,
			};
		}
	}
}
