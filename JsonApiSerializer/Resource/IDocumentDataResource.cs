using System.Collections.Generic;

namespace DKX.JsonApiSerializer.Resource
{
	public interface IDocumentDataResource
	{
		IEnumerable<Item> GetIncludedItems();
	}
}
