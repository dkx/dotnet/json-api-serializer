using System;
using System.Collections.Immutable;

namespace DKX.JsonApiSerializer.Resource
{
	public class Document : ICloneable
	{
		public Document(IDocumentDataResource? data = null)
		{
			Data = data;
		}
		
		public IDocumentDataResource? Data { get; }

		public Meta? Metadata { get; private set; }

		public ErrorsList? Errors { get; private set; }

		public Document WithMeta(Meta? metadata)
		{
			var clone = (Document) Clone();
			clone.Metadata = metadata;

			return clone;
		}

		public Document WithErrors(ErrorsList? errors)
		{
			var clone = (Document) Clone();
			clone.Errors = errors;

			return clone;
		}

		public Document AddError(Error error)
		{
			var errors = Errors ?? new ErrorsList(ImmutableList<Error>.Empty);

			return WithErrors(errors.AddError(error));
		}

		public object Clone()
		{
			return new Document(Data)
			{
				Metadata = Metadata,
				Errors = Errors,
			};
		}
	}
}
