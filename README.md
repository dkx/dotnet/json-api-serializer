# DKX/JsonApiSerializer

.NET Core serializer for [json:api](https://jsonapi.org/)

## Installation

```bash
$ dotnet add package DKX.JsonApiSerializer
```

## Example

```c#
using DKX.JsonApiSerializer;
using DKX.JsonApiSerializer.Resource;
using System.Text.Json;

var document = new Document(
    new Collection(new []
    {
        new Item("book", "98437")
            .AddAttribute("name", "Harry Potter")
            .AddRelationship(
                "author",
                new Item("user", "327")
                    .AddAttribute("name", "J. K. Rowling")
            )
    })
);

var serializer = new Serializer(new SerializationOptions
{
    JsonOptions = new JsonWriterOptions
    {
        Indented = true,
    },
});
```

Output:

```json
{
    "data": [
        {
            "type": "book",
            "id": "98437",
            "attributes": {
                "name": "Harry Potter"
            },
            "relationships": {
                "author": {
                    "data": {
                        "type": "user",
                        "id": "327"
                    }
                }
            }
        }
    ],
    "included": [
        {
            "type": "user",
            "id": "327",
            "attributes": {
                "name": "J. K. Rowling"
            }
        }
    ]
}
```

Look at tests for more examples.
