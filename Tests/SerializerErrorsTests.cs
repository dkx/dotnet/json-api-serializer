using DKX.JsonApiSerializer;
using DKX.JsonApiSerializer.Resource;
using Xunit;

namespace DKX.JsonApiSerializerTests
{
	public class SerializerErrorsTests
	{
		private readonly Serializer _serializer = SerializerFactory.Create();

		[Fact]
		public void Serialize_ErrorsList()
		{
			var doc = new Document()
				.AddError(
					new Error()
						.WithId("E123")
						.WithAboutLink("http://localhost/error/E123")
						.WithStatus("400")
						.WithCode("REQUIRED")
						.WithTitle("Title is required")
						.WithDetail("Title field is required")
						.WithSourcePointer("/query/title")
						.WithSourceParameter("title")
						.WithMetadata(
							new Meta()
								.Add("num", 1U)
						)
				);

			Assert.Equal(
				@"{
  ""errors"": [
    {
      ""id"": ""E123"",
      ""links"": {
        ""about"": ""http://localhost/error/E123""
      },
      ""status"": ""400"",
      ""code"": ""REQUIRED"",
      ""title"": ""Title is required"",
      ""detail"": ""Title field is required"",
      ""source"": {
        ""pointer"": ""/query/title"",
        ""parameter"": ""title""
      },
      ""meta"": {
        ""num"": 1
      }
    }
  ]
}",
				_serializer.Serialize(doc)
			);
		}
	}
}
