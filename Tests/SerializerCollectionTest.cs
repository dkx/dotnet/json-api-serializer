using DKX.JsonApiSerializer;
using DKX.JsonApiSerializer.Relationship;
using DKX.JsonApiSerializer.Resource;
using Xunit;

namespace DKX.JsonApiSerializerTests
{
	public class SerializerCollectionTest
	{
		private readonly Serializer _serializer = SerializerFactory.Create();

		[Fact]
		public void Serialize_Collection()
		{
			var doc = new Document(new Collection(new[]
			{
				new Item("book", "5")
					.AddAttribute("title", "Harry Potter"),
			}));

			Assert.Equal(
				@"{
  ""data"": [
    {
      ""type"": ""book"",
      ""id"": ""5"",
      ""attributes"": {
        ""title"": ""Harry Potter""
      }
    }
  ]
}",
				_serializer.Serialize(doc)
			);
		}

		[Fact]
		public void Serialize_Collection_Relationship_Null()
		{
			var doc = new Document(new Collection(new []
			{
				new Item("book", "5")
					.AddRelationship("user", new NullRelationship()),
			}));

			Assert.Equal(
				@"{
  ""data"": [
    {
      ""type"": ""book"",
      ""id"": ""5"",
      ""relationships"": {
        ""user"": {
          ""data"": null
        }
      }
    }
  ]
}",
				_serializer.Serialize(doc)
			);
		}

		[Fact]
		public void Serialize_Collection_Relationship_Item()
		{
			var doc = new Document(new Collection(new[]
			{
				new Item("book", "5")
					.AddRelationship(
						"user",
						new Item("user", "10")
							.AddAttribute("name", "John Doe")
					), 
			}));

			Assert.Equal(
				@"{
  ""data"": [
    {
      ""type"": ""book"",
      ""id"": ""5"",
      ""relationships"": {
        ""user"": {
          ""data"": {
            ""type"": ""user"",
            ""id"": ""10""
          }
        }
      }
    }
  ],
  ""included"": [
    {
      ""type"": ""user"",
      ""id"": ""10"",
      ""attributes"": {
        ""name"": ""John Doe""
      }
    }
  ]
}",
				_serializer.Serialize(doc)
			);
		}

		[Fact]
		public void Serialize_Collection_Relationship_Item_Nested()
		{
			var doc = new Document(new Collection(new[]
			{
				new Item("book", "5")
					.AddRelationship(
						"user",
						new Item("user", "10")
							.AddAttribute("name", "John Doe")
							.AddRelationship(
								"role",
								new Item("role", "admin")
									.AddAttribute("title", "Admin")
							)
						),
			}));

			Assert.Equal(
				@"{
  ""data"": [
    {
      ""type"": ""book"",
      ""id"": ""5"",
      ""relationships"": {
        ""user"": {
          ""data"": {
            ""type"": ""user"",
            ""id"": ""10""
          }
        }
      }
    }
  ],
  ""included"": [
    {
      ""type"": ""role"",
      ""id"": ""admin"",
      ""attributes"": {
        ""title"": ""Admin""
      }
    },
    {
      ""type"": ""user"",
      ""id"": ""10"",
      ""attributes"": {
        ""name"": ""John Doe""
      },
      ""relationships"": {
        ""role"": {
          ""data"": {
            ""type"": ""role"",
            ""id"": ""admin""
          }
        }
      }
    }
  ]
}",
				_serializer.Serialize(doc)
			);
		}

		[Fact]
		public void Serialize_Collection_Relationship_Item_Duplicates()
		{
			var user = new ItemRelationship(
				new Item("user", "10")
					.AddAttribute("name", "John Doe")
			);
			
			var doc = new Document(new Collection(new[]
			{
				new Item("book", "5")
					.AddRelationship("user", user),
				new Item("book", "15")
					.AddRelationship("user", user),
			}));

			Assert.Equal(
				@"{
  ""data"": [
    {
      ""type"": ""book"",
      ""id"": ""15"",
      ""relationships"": {
        ""user"": {
          ""data"": {
            ""type"": ""user"",
            ""id"": ""10""
          }
        }
      }
    },
    {
      ""type"": ""book"",
      ""id"": ""5"",
      ""relationships"": {
        ""user"": {
          ""data"": {
            ""type"": ""user"",
            ""id"": ""10""
          }
        }
      }
    }
  ],
  ""included"": [
    {
      ""type"": ""user"",
      ""id"": ""10"",
      ""attributes"": {
        ""name"": ""John Doe""
      }
    }
  ]
}",
				_serializer.Serialize(doc)
			);
		}

		[Fact]
		public void Serialize_Collection_Relationship_Collection()
		{
			var doc = new Document(new Collection(new[]
			{
				new Item("book", "5")
					.AddRelationship(
						"users",
						new[]
						{
							new Item("user", "10")
								.AddAttribute("name", "John Doe")
						}
					), 
			}));

			Assert.Equal(
				@"{
  ""data"": [
    {
      ""type"": ""book"",
      ""id"": ""5"",
      ""relationships"": {
        ""users"": {
          ""data"": [
            {
              ""type"": ""user"",
              ""id"": ""10""
            }
          ]
        }
      }
    }
  ],
  ""included"": [
    {
      ""type"": ""user"",
      ""id"": ""10"",
      ""attributes"": {
        ""name"": ""John Doe""
      }
    }
  ]
}",
				_serializer.Serialize(doc)
			);
		}
	}
}
