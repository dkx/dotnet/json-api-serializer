using DKX.JsonApiSerializer.Value;
using Xunit;

namespace DKX.JsonApiSerializerTests.Value
{
	public class StringValueTest
	{
		[Theory]
		[InlineData("lorem ipsum")]
		public void Value(string value)
		{
			var data = new StringValue(value);

			Assert.Equal(value, data.Value);
		}
	}
}
