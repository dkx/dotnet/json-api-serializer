using System;
using DKX.JsonApiSerializer.Resource;
using DKX.JsonApiSerializer.Value;
using Xunit;

namespace DKX.JsonApiSerializerTests.Value
{
	public class ValueFactoryTest
	{
		[Fact]
		public void Create_Throws_NotSupported()
		{
			var ex = Assert.Throws<NotSupportedException>(() => ValueFactory.Create(new DateTime()));

			Assert.Equal("Type System.DateTime is not supported", ex.Message);
		}

		[Fact]
		public void Create_Null()
		{
			string? value = null;
			var result = ValueFactory.Create(value);

			Assert.IsType<Null>(result);
		}

		[Fact]
		public void Create_Object_Bool()
		{
			object? value = true;
			var result = ValueFactory.Create(value);

			Assert.IsType<BooleanValue>(result);
			Assert.True(((BooleanValue) result).Value);
		}

		[Fact]
		public void Create_Object_Decimal()
		{
			object? value = 5M;
			var result = ValueFactory.Create(value);

			Assert.IsType<NumberValue>(result);
			Assert.Equal(5M, ((NumberValue) result).Value);
		}

		[Fact]
		public void Create_Object_Double()
		{
			object? value = 5D;
			var result = ValueFactory.Create(value);

			Assert.IsType<NumberValue>(result);
			Assert.Equal(5D, ((NumberValue) result).Value);
		}

		[Fact]
		public void Create_Object_Int()
		{
			object? value = 5;
			var result = ValueFactory.Create(value);

			Assert.IsType<NumberValue>(result);
			Assert.Equal(5, ((NumberValue) result).Value);
		}

		[Fact]
		public void Create_Object_Long()
		{
			object? value = 5L;
			var result = ValueFactory.Create(value);

			Assert.IsType<NumberValue>(result);
			Assert.Equal(5L, ((NumberValue) result).Value);
		}

		[Fact]
		public void Create_Object_Float()
		{
			object? value = 5.5;
			var result = ValueFactory.Create(value);

			Assert.IsType<NumberValue>(result);
			Assert.Equal(5.5, ((NumberValue) result).Value);
		}

		[Fact]
		public void Create_Object_Uint()
		{
			object? value = 5U;
			var result = ValueFactory.Create(value);

			Assert.IsType<NumberValue>(result);
			Assert.Equal(5U, ((NumberValue) result).Value);
		}

		[Fact]
		public void Create_Object_Ulong()
		{
			object? value = 5UL;
			var result = ValueFactory.Create(value);

			Assert.IsType<NumberValue>(result);
			Assert.Equal(5UL, ((NumberValue) result).Value);
		}

		[Fact]
		public void Create_Object_String()
		{
			object? value = "test";
			var result = ValueFactory.Create(value);

			Assert.IsType<StringValue>(result);
			Assert.Equal("test", ((StringValue) result).Value);
		}

		[Fact]
		public void Create_ImmediateReturn()
		{
			var value = new StringValue("hello world");
			var result = ValueFactory.Create(value);

			Assert.Same(result, value);
		}
	}
}
