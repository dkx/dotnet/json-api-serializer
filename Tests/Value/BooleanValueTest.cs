using DKX.JsonApiSerializer.Value;
using Xunit;

namespace DKX.JsonApiSerializerTests.Value
{
	public class BooleanValueTest
	{
		[Theory]
		[InlineData(true)]
		[InlineData(false)]
		public void Value(bool value)
		{
			var data = new BooleanValue(value);

			Assert.Equal(value, data.Value);
		}
	}
}
