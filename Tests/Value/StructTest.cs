using DKX.JsonApiSerializer;
using DKX.JsonApiSerializer.Resource;
using DKX.JsonApiSerializer.Value;
using Xunit;

namespace DKX.JsonApiSerializerTests.Value
{
	public class StructTest
	{
		private readonly Serializer _serializer = SerializerFactory.Create();
		
		[Fact]
		public void WriteToJson()
		{
			int? nullable = null;
			
			var value = new Struct()
				.Add("string", "Hello world")
				.Add("number", 42u)
				.Add("boolean", true)
				.Add("null", nullable)
				.Add(
					"inner-a",
					new Struct()
						.Add("name", "inner-1")
						.Add(
							"inner-b",
							new Struct()
								.Add("name", "inner-2")
								.Add(
									"list",
									new List()
										.Add("lorem ipsum")
										.Add(84d)
										.Add(false)
										.Add(new Null())
										.Add(
											new Struct()
												.Add("name", "struct-in-list")
										)
										.Add(
											new List()
												.Add("list-in-list")
										)
								)
						)
				);

			var json = _serializer.Serialize(new Document(new Item("book", "42").WithAttributes(value)));

			Assert.Equal(
				@"{
  ""data"": {
    ""type"": ""book"",
    ""id"": ""42"",
    ""attributes"": {
      ""boolean"": true,
      ""inner-a"": {
        ""inner-b"": {
          ""list"": [
            ""lorem ipsum"",
            84,
            false,
            null,
            {
              ""name"": ""struct-in-list""
            },
            [
              ""list-in-list""
            ]
          ],
          ""name"": ""inner-2""
        },
        ""name"": ""inner-1""
      },
      ""null"": null,
      ""number"": 42,
      ""string"": ""Hello world""
    }
  }
}",
				json
			);
		}

		[Fact]
		public void Merge()
		{
			var a = new Struct().Add("a", "A");
			var b = new Struct().Add("b", "B");
			var result = a.Merge(b).Data;

			Assert.Equal(2, result.Count);
			Assert.Contains("a", result);
			Assert.Contains("b", result);
			Assert.Equal("A", ((StringValue) result["a"]).Value);
			Assert.Equal("B", ((StringValue) result["b"]).Value);
		}
	}
}
