using System.Collections.Generic;
using DKX.JsonApiSerializer.Value;
using Xunit;

namespace DKX.JsonApiSerializerTests.Value
{
	public class NumberValueTest
	{
		[Theory]
		[MemberData(nameof(GetNumbers))]
		public void Value(dynamic value)
		{
			var data = new NumberValue(value);

			Assert.Equal(value, data.Value);
		}

		public static IEnumerable<object[]> GetNumbers()
		{
			yield return new object[] {42};
			yield return new object[] {42.0};
			yield return new object[] {42.0F};
			yield return new object[] {42.0M};
			yield return new object[] {42U};
			yield return new object[] {42L};
			yield return new object[] {42UL};
		}
	}
}
