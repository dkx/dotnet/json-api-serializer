using System.Collections.Immutable;
using DKX.JsonApiSerializer.Value;
using Xunit;

namespace DKX.JsonApiSerializerTests.Value
{
	public class ListTest
	{
		[Fact]
		public void Merge()
		{
			var a = new List().Add("a");
			var b = new List().Add("b");
			var result = a.Merge(b).Data.ToImmutableArray();

			Assert.Equal(2, result.Length);
			Assert.Equal("a", ((StringValue) result[0]).Value);
			Assert.Equal("b", ((StringValue) result[1]).Value);
		}
	}
}
