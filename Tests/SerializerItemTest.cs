using System.Diagnostics;
using DKX.JsonApiSerializer;
using DKX.JsonApiSerializer.Relationship;
using DKX.JsonApiSerializer.Resource;
using DKX.JsonApiSerializer.Value;
using Xunit;

namespace DKX.JsonApiSerializerTests
{
	public class SerializerItemTest
	{
		private readonly Serializer _serializer = SerializerFactory.Create();

		[Fact]
		public void Serialize_Item()
		{
			var doc = new Document(
				new Item("book", "5")
					.WithSelfLink("http://localhost/books/5")
					.AddAttribute("title", "Harry Potter")
			);

			Assert.Equal(
				@"{
  ""data"": {
    ""type"": ""book"",
    ""id"": ""5"",
    ""links"": {
      ""self"": ""http://localhost/books/5""
    },
    ""attributes"": {
      ""title"": ""Harry Potter""
    }
  }
}",
				_serializer.Serialize(doc)
			);
		}

		[Fact]
		public void Serialize_Item_Relationship_Null()
		{
			var doc = new Document(
				new Item("book", "5")
					.AddRelationship("user", new NullRelationship())
				);

			Assert.Equal(
				@"{
  ""data"": {
    ""type"": ""book"",
    ""id"": ""5"",
    ""relationships"": {
      ""user"": {
        ""data"": null
      }
    }
  }
}",
				_serializer.Serialize(doc)
			);
		}

		[Fact]
		public void Serialize_Item_Relationship_Item()
		{
			var doc = new Document(
				new Item("book", "5")
					.AddRelationship(
						"user",
						new Item("user", "10")
							.AddAttribute("name", "John Doe")
					)
			);

			Assert.Equal(
				@"{
  ""data"": {
    ""type"": ""book"",
    ""id"": ""5"",
    ""relationships"": {
      ""user"": {
        ""data"": {
          ""type"": ""user"",
          ""id"": ""10""
        }
      }
    }
  },
  ""included"": [
    {
      ""type"": ""user"",
      ""id"": ""10"",
      ""attributes"": {
        ""name"": ""John Doe""
      }
    }
  ]
}",
				_serializer.Serialize(doc)
			);
		}

		[Fact]
		public void Serialize_Item_Relationship_Item_Nested()
		{
			var doc = new Document(
				new Item("book", "5")
					.AddRelationship(
						"user",
						new Item("user", "10")
							.AddAttribute("name", "John Doe")
							.AddRelationship(
								"role",
								new Item("role", "admin")
									.AddAttribute("title", "Admin")
							)
					)
				);

			Assert.Equal(
				@"{
  ""data"": {
    ""type"": ""book"",
    ""id"": ""5"",
    ""relationships"": {
      ""user"": {
        ""data"": {
          ""type"": ""user"",
          ""id"": ""10""
        }
      }
    }
  },
  ""included"": [
    {
      ""type"": ""role"",
      ""id"": ""admin"",
      ""attributes"": {
        ""title"": ""Admin""
      }
    },
    {
      ""type"": ""user"",
      ""id"": ""10"",
      ""attributes"": {
        ""name"": ""John Doe""
      },
      ""relationships"": {
        ""role"": {
          ""data"": {
            ""type"": ""role"",
            ""id"": ""admin""
          }
        }
      }
    }
  ]
}",
				_serializer.Serialize(doc)
			);
		}

		[Fact]
		public void Serialize_Item_Relationship_Collection()
		{
			var doc = new Document(
				new Item("book", "5")
					.AddRelationship(
						"users",
						new []
						{
							new Item("user", "10")
								.AddAttribute("name", "John Doe"),
							new Item("user", "20")
								.AddAttribute("name", "Lord Voldemort")
						}
					)
				);

			Assert.Equal(
				@"{
  ""data"": {
    ""type"": ""book"",
    ""id"": ""5"",
    ""relationships"": {
      ""users"": {
        ""data"": [
          {
            ""type"": ""user"",
            ""id"": ""10""
          },
          {
            ""type"": ""user"",
            ""id"": ""20""
          }
        ]
      }
    }
  },
  ""included"": [
    {
      ""type"": ""user"",
      ""id"": ""10"",
      ""attributes"": {
        ""name"": ""John Doe""
      }
    },
    {
      ""type"": ""user"",
      ""id"": ""20"",
      ""attributes"": {
        ""name"": ""Lord Voldemort""
      }
    }
  ]
}",
				_serializer.Serialize(doc)
			);
		}

		[Fact]
		public void Serialize_Item_Relationship_Collection_Duplicates()
		{
			var role = new Item("role", "admin")
				.AddAttribute("title", "Admin");
			
			var doc = new Document(
				new Item("book", "5")
					.AddRelationship(
						"users",
						new[]
						{
							new Item("user", "10")
								.AddAttribute("name", "John Doe")
								.AddRelationship("role", role),
							new Item("user", "20")
								.AddAttribute("name", "Lord Voldemort")
								.AddRelationship("role", role)
						}
					)
				);

			Assert.Equal(
				@"{
  ""data"": {
    ""type"": ""book"",
    ""id"": ""5"",
    ""relationships"": {
      ""users"": {
        ""data"": [
          {
            ""type"": ""user"",
            ""id"": ""10""
          },
          {
            ""type"": ""user"",
            ""id"": ""20""
          }
        ]
      }
    }
  },
  ""included"": [
    {
      ""type"": ""role"",
      ""id"": ""admin"",
      ""attributes"": {
        ""title"": ""Admin""
      }
    },
    {
      ""type"": ""user"",
      ""id"": ""10"",
      ""attributes"": {
        ""name"": ""John Doe""
      },
      ""relationships"": {
        ""role"": {
          ""data"": {
            ""type"": ""role"",
            ""id"": ""admin""
          }
        }
      }
    },
    {
      ""type"": ""user"",
      ""id"": ""20"",
      ""attributes"": {
        ""name"": ""Lord Voldemort""
      },
      ""relationships"": {
        ""role"": {
          ""data"": {
            ""type"": ""role"",
            ""id"": ""admin""
          }
        }
      }
    }
  ]
}",
				_serializer.Serialize(doc)
			);
		}

		[Fact]
		public void Serialize_Deep_Relationships()
		{
			var doc = new Document(
				new Item("user", "10")
					.AddRelationship(
						"projects",
						new CollectionRelationship(
							new []
							{
								new ItemRelationship(
									new Item("project", "20")
										.AddRelationship(
											"owner",
											new ItemRelationship(
												new Item("user", "11")
													.AddRelationship(
														"role",
														new ItemRelationship(
															new Item("user_role", "30")
														)
													)
											)
										)
								), 
							}
						) 
					)
			);

			var json = _serializer.Serialize(doc);

			Assert.Equal(@"{
  ""data"": {
    ""type"": ""user"",
    ""id"": ""10"",
    ""relationships"": {
      ""projects"": {
        ""data"": [
          {
            ""type"": ""project"",
            ""id"": ""20""
          }
        ]
      }
    }
  },
  ""included"": [
    {
      ""type"": ""project"",
      ""id"": ""20"",
      ""relationships"": {
        ""owner"": {
          ""data"": {
            ""type"": ""user"",
            ""id"": ""11""
          }
        }
      }
    },
    {
      ""type"": ""user"",
      ""id"": ""11"",
      ""relationships"": {
        ""role"": {
          ""data"": {
            ""type"": ""user_role"",
            ""id"": ""30""
          }
        }
      }
    },
    {
      ""type"": ""user_role"",
      ""id"": ""30""
    }
  ]
}", json);
		}
	}
}
