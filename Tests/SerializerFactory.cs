using System.Text.Json;
using DKX.JsonApiSerializer;

namespace DKX.JsonApiSerializerTests
{
	public static class SerializerFactory
	{
		public static Serializer Create()
		{
			return new Serializer(new SerializationOptions
			{
				Sorted = true,
				JsonOptions = new JsonWriterOptions
				{
					Indented = true,
				},
			});
		}
	}
}
