using DKX.JsonApiSerializer;
using DKX.JsonApiSerializer.Resource;
using Xunit;

namespace DKX.JsonApiSerializerTests
{
	public class SerializerTest
	{
		private readonly Serializer _serializer = SerializerFactory.Create();
		
		[Fact]
		public void Serialize_Document_Empty()
		{
			var doc = new Document();

			Assert.Equal("{}", _serializer.Serialize(doc));
		}

		[Fact]
		public void Serialize_Document_Null()
		{
			var doc = new Document(new Null());
			
			Assert.Equal(
				@"{
  ""data"": null
}",
				_serializer.Serialize(doc)
			);
		}

		[Fact]
		public void Serialize_Document_Meta()
		{
			var doc = new Document()
				.WithMeta(
					new Meta()
						.Add("authenticated", true)
				);

			Assert.Equal(
				@"{
  ""meta"": {
    ""authenticated"": true
  }
}",
				_serializer.Serialize(doc)
			);
		}
	}
}
