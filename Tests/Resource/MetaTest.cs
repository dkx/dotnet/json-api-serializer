using DKX.JsonApiSerializer.Resource;
using DKX.JsonApiSerializer.Value;
using Xunit;

namespace DKX.JsonApiSerializerTests.Resource
{
	public class MetaTest
	{
		[Fact]
		public void Merge()
		{
			var a = new Meta(new Struct().Add("a", "A"));
			var b = new Meta(new Struct().Add("b", "B"));
			var result = a.Merge(b).Fields.Data;
			
			Assert.Equal(2, result.Count);
			Assert.Contains("a", result);
			Assert.Contains("b", result);
			Assert.Equal("A", ((StringValue) result["a"]).Value);
			Assert.Equal("B", ((StringValue) result["b"]).Value);
		}
	}
}
